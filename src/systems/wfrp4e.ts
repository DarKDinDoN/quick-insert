// Warhammer Fantasy Roleplay 4th edition integration
import { CharacterSheetContext } from "../module/contexts";
import { QuickInsert } from "../module/core";
import { getSetting, setSetting } from "../module/settings";
import { ModuleSetting } from "../module/store/ModuleSettings";

export const SYSTEM_NAME = "wfrp4e";

export const defaultSheetFilters = {
  career: "wfrp4e.careers",
  skill: "wfrp4e.skills",
  talent: "wfrp4e.talents",
  injury: "wfrp4e.injuries",
  critical: "wfrp4e.criticals",
  weapon: "wfrp4e.trappings",
  trapping: "wfrp4e.trappings",
  spell: "wfrp4e.spells",
  prayer: "wfrp4e.prayers",
  psychology: "wfrp4e.psychologies",
  mutation: "wfrp4e.mutations",
  disease: "wfrp4e.diseases",
};

export class Wfrp4eSheetContext extends CharacterSheetContext {
  constructor(
    documentSheet: DocumentSheet,
    anchor: JQuery<HTMLElement>,
    sheetType?: string,
    insertType?: string
  ) {
    super(documentSheet, anchor);
    this.spawnCSS = {
      ...this.spawnCSS,
      left: (this.spawnCSS?.left as number) - 10,
      bottom: (this.spawnCSS?.bottom as number) + 10,
    };
    if (sheetType && insertType) {
      const sheetFilters = getSetting(ModuleSetting.FILTERS_SHEETS).baseFilters;
      this.filter =
        sheetFilters[`${sheetType}.${insertType}`] || sheetFilters[insertType];
    }
  }
}

export function sheetWfrp4eRenderHook(
  app: DocumentSheet,
  sheetType?: string
): void {
  if (app.element.find(".quick-insert-link").length > 0) {
    return;
  }
  const link = `<a class="quick-insert-link" title="Quick Insert"><i class="fas fa-search"></i></a>`;
  app.element.find("a.item-create").each((i, el) => {
    const type = el.dataset.type || "";
    if (!Object.keys(defaultSheetFilters).includes(type)) return;
    const linkEl = $(link);
    $(el).after(linkEl);
    linkEl.on("click", () => {
      const context = new Wfrp4eSheetContext(app, linkEl, sheetType, type);
      QuickInsert.open(context);
    });
  });
}

export function init(): void {
  if (game.user?.isGM) {
    const customFilters = getSetting(ModuleSetting.FILTERS_SHEETS).baseFilters;
    setSetting(ModuleSetting.FILTERS_SHEETS, {
      baseFilters: {
        ...defaultSheetFilters,
        ...customFilters,
      },
    });
  }
  Hooks.on("renderActorSheetWfrp4eCharacter", (app: DocumentSheet) => {
    getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED) &&
      sheetWfrp4eRenderHook(app, "character");
  });

  console.log("Quick Insert | wfrp4e system extensions initiated");
}
