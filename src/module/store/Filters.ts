export enum FilterType {
  Default,
  World,
  Client,
}

export interface SearchFilterConfig {
  compendiums: string[] | "any";
  folders: string[] | "any";
  entities: string[] | "any";
}

export interface SearchFilter {
  id: string;
  type: FilterType;
  tag: string; // used with @tag
  subTitle: string; // Title
  filterConfig?: SearchFilterConfig;
  disabled?: boolean;
}

export interface WorldSavedFilters {
  filters: SearchFilter[];
}

export interface ClientSavedFilters {
  disabled: string[];
  filters: SearchFilter[];
}
