import { QuickInsert } from "./core";
import { getSetting, setSetting } from "./settings";
import { ModuleSetting } from "./store/ModuleSettings";
import { i18n } from "./utils";

export class SheetFilters extends FormApplication {
  get element(): JQuery<HTMLElement> {
    return super.element as JQuery<HTMLElement>;
  }

  static get defaultOptions(): FormApplicationOptions {
    return {
      ...super.defaultOptions,
      title: i18n("SheetFiltersTitle"),
      id: "sheet-filters",
      template: "modules/quick-insert/templates/sheet-filters.hbs",
      resizable: true,
    };
  }

  getData(): any {
    const filters = QuickInsert.filters.filters;
    const customFilters = getSetting(ModuleSetting.FILTERS_SHEETS).baseFilters;

    return {
      filters: Object.entries(customFilters).map(([key, filter]) => ({
        key,
        noFilter: filter === "",
        options: filters.map((f) => ({
          ...f,
          selected: filter === f.tag || filter === f.id,
        })),
      })),
    };
  }

  activateListeners(html: JQuery<HTMLElement>): void {
    super.activateListeners(html);
  }

  async _updateObject(event: unknown, formData: unknown): Promise<void> {
    setSetting(ModuleSetting.FILTERS_SHEETS, {
      baseFilters: formData,
    });
  }
}
