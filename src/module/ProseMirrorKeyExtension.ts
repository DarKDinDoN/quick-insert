import { ProseMirrorContext } from "./contexts";
import { QuickInsert } from "./core";
import { ModuleSetting } from "./store/ModuleSettings";

declare const ProseMirror: any;

function mapKey(key: string) {
  if (key.startsWith("Key")) {
    return key[key.length - 1].toLowerCase();
  }
  return key;
}

export function registerProseMirrorKeys() {
  const binds = game?.keybindings?.bindings?.get(
    "quick-insert." + ModuleSetting.KEY_BIND
  );

  if (!binds?.length) {
    console.info("Quick Insert | ProseMirror extension found no key binding");
    return;
  }

  function keyCallback(state: any, dispatch: any, view: any) {
    if (QuickInsert.app?.embeddedMode) return false;

    // Open window
    QuickInsert.open(new ProseMirrorContext(state, dispatch, view));
    return true;
  }

  const keyMap = Object.fromEntries(
    binds.map((bind) => {
      return [
        `${bind.modifiers?.map((m) => m + "-").join("")}${mapKey(bind.key)}`,
        keyCallback,
      ];
    })
  );

  ProseMirror.defaultPlugins.QuickInsert = ProseMirror.keymap(keyMap);
}
