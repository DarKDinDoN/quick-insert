import { getSetting, registerMenu } from "module/settings";
import { setupDocumentHooks } from "module/documentHooks";

import { FilterList } from "module/filterList";
import { SheetFilters } from "module/sheetFilters";

import { importSystemIntegration } from "module/systemIntegration";
import { registerTinyMCEPlugin } from "module/tinyMCEPlugin";

import { SearchApp } from "module/searchApp";
import { QuickInsert, loadSearchIndex } from "module/core";
import { SearchFilterCollection } from "module/searchFilters";
import { IndexingSettings } from "module/indexingSettings";
import { customKeybindHandler, resolveAfter } from "module/utils";
import { ModuleSetting } from "module/store/ModuleSettings";
import { registerSettings } from "module/store/SettingsRegistry";
import { registerProseMirrorKeys } from "module/ProseMirrorKeyExtension";

function quickInsertDisabled(): boolean {
  return !game.user?.isGM && getSetting(ModuleSetting.GM_ONLY);
}

// Client is currently reindexing?
let reIndexing = false;

Hooks.once("init", async function () {
  registerMenu({
    menu: "indexingSettings",
    name: "QUICKINSERT.SettingsIndexingSettings",
    label: "QUICKINSERT.SettingsIndexingSettingsLabel",
    icon: "fas fa-search",
    type: IndexingSettings as ConstructorOf<FormApplication>,
    restricted: false,
  });
  registerMenu({
    menu: "filterMenu",
    name: "QUICKINSERT.SettingsFilterMenu",
    label: "QUICKINSERT.SettingsFilterMenuLabel",
    icon: "fas fa-filter",
    type: FilterList as ConstructorOf<FormApplication>,
    restricted: false,
  });
  registerSettings({
    [ModuleSetting.FILTERS_WORLD]: () => {
      if (quickInsertDisabled()) return;
      QuickInsert.filters.loadSave();
    },
    [ModuleSetting.FILTERS_CLIENT]: () => {
      if (quickInsertDisabled()) return;
      QuickInsert.filters.loadSave();
    },
    [ModuleSetting.INDEXING_DISABLED]: async () => {
      if (quickInsertDisabled()) return;
      // Active users will start reindexing in deterministic order, once per 300ms

      if (reIndexing) return;
      reIndexing = true;
      if (game.users && game.userId !== null) {
        const order = [...game.users.contents]
          .filter((u) => u.active)
          .map((u) => u.id)
          .indexOf(game.userId);
        await resolveAfter(order * 300);
      }
      await QuickInsert.forceIndex();
      reIndexing = false;
    },
  });

  game.keybindings.register("quick-insert", ModuleSetting.KEY_BIND, {
    name: "QUICKINSERT.SettingsQuickOpen",
    textInput: true,
    editable: [
      { key: "Space", modifiers: [KeyboardManager.MODIFIER_KEYS.CONTROL] },
    ],
    onDown: (ctx: any) => {
      QuickInsert.toggle(ctx._quick_insert_extra?.context);
      return true;
    },
    precedence: CONST.KEYBINDING_PRECEDENCE.NORMAL,
  });
});

Hooks.once("ready", function () {
  if (quickInsertDisabled()) return;
  console.log("Quick Insert | Initializing...");

  // Initialize application base
  QuickInsert.filters = new SearchFilterCollection();
  QuickInsert.app = new SearchApp();

  registerTinyMCEPlugin();
  registerProseMirrorKeys();

  importSystemIntegration().then((systemIntegration) => {
    if (systemIntegration) {
      QuickInsert.systemIntegration = systemIntegration;
      QuickInsert.systemIntegration.init();

      if (QuickInsert.systemIntegration.defaultSheetFilters) {
        registerMenu({
          menu: "sheetFilters",
          name: "QUICKINSERT.SettingsSheetFilters",
          label: "QUICKINSERT.SettingsSheetFiltersLabel",
          icon: "fas fa-filter",
          type: SheetFilters as ConstructorOf<FormApplication>,
          restricted: false,
        });
      }
    }
  });

  document.addEventListener("keydown", (evt) => {
    // Allow in input fields...
    customKeybindHandler(evt);
  });

  setupDocumentHooks(QuickInsert);

  console.log("Quick Insert | Search Application ready");

  const indexDelay = getSetting(ModuleSetting.AUTOMATIC_INDEXING);
  if (indexDelay != -1) {
    setTimeout(() => {
      console.log("Quick Insert | Automatic indexing initiated");
      loadSearchIndex();
    }, indexDelay);
  }
});

Hooks.on("renderSceneControls", (controls: unknown, html: JQuery) => {
  if (!getSetting(ModuleSetting.SEARCH_BUTTON)) return;
  const searchBtn = $(
    `<li class="scene-control" title="Quick Insert" class="quick-insert-open">
          <i class="fas fa-search"></i>
      </li>`
  );
  html.children(".main-controls").append(searchBtn);
  searchBtn.on("click", () => QuickInsert.open());
});

// Exports and API usage
//@ts-ignore
globalThis.QuickInsert = QuickInsert;
export { SearchContext } from "module/contexts";
export { QuickInsert };
